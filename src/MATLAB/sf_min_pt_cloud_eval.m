% MIT License
% 
% Copyright (c) 2021 Viktorio S. el Hakim
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

%
% This script evaluates the function minsf_pc on randomly generated data 
%

Times = [];
Ds = [];
ns = [];
N = 1000;
for n = 2:20
    for nt = 1:3
        fprintf('n = %d, Trial %d\n',n,nt);

        % The point cloud
        %Mix uniform+normal
%         P = [5*rand(n,N)-2.5 , randn(n,N)]; 
        %Just uniform
        % P = 5*rand(n,N)-2.5;

        %Randomly rotated uniform
        C = 5*rand(n,N)-2.5;
        %Indexing pattern for off-diagonal entries of matrix
        idx = nchoosek(1:n,2);
        dU = zeros(n);
        dth = pi*(2*rand(n*(n-1)/2,1) - 1); %Uniform
        %     dth = pfac*randn(n*(n-1)/2,Npart);
        dU(sub2ind([n,n],idx(:,1),idx(:,2))) = dth;
        dU(sub2ind([n,n],idx(:,2),idx(:,1))) = -dth;
        C = expm(dU) * C;

        % figure(1)
        % plot(P(1,:),P(2,:),'ro');


    %     [x,fopt1] = minsf_pc(P,5);
    %     [x,fopt2] = minsf_pc(P,5);
    %     [x,fopt3] = minsf_pc(P,5);
    %     fopt = min([fopt1,fopt2,fopt3]);

        tstart = tic;
        [xopt,fopt] = minsf_pc(C,5);
        telapsed = toc(tstart);
        Times = [Times,telapsed];
        

        fprintf('Generating test data...\n');

        K = 1000000;
        O = randn(n,K);
        f = cellfun(@(v) max(C'*v/norm(v)),num2cell(O,1));
        Ds = [Ds,min(f)-fopt];
        
        ns = [ns,n];

        fprintf('Done...\n');
    end
end

figure(1)
plot(ns,Times,'bv');
xlabel('$n$','Interpreter','latex');
ylabel('$T [s]$','Interpreter','latex');
grid on

figure(2)
plot(ns,Ds,'bv');
xlabel('$n$','Interpreter','latex');
ylabel('$d$','Interpreter','latex');
grid on


figure(3)
plot(f,'-r')
hold on
plot([1,size(f,2)],[fopt,fopt],'-b');
hold off
xlabel('$k$','Interpreter','latex');
ylabel('$f(x)$','Interpreter','latex');
legend({'$f(x_k)$','$f^*$'},'Interpreter','latex')
grid on
