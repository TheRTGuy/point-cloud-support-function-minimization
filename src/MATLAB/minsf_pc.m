function [ xopt, fval ] = minsf_pc( P, Niter )
% minsf_pc Point-cloud support function minimization.
% [ xopt, fval ] = minsf_pc( P, Niter ) finds an approximate solution to
% the problem:
%               min max{p_1'*x,...,p_k'*x}    subject to: ||x||_2 = 1,
%                x
% where {p_1,...,p_k} is a set of k n-dimensional points, such that
% Conv({p_1,...,p_k}) contains the origin 0. In other words, it minimizes
% the support function of the point cloud. The points are specified in
% matrix form P = [p_1 .... p_k], with each p_i a n-by-1 vector. 
%
% Nitr specifies the number of iterations. xopt is the sub-optimal
% solution, and fval is the sub-optimal objective value.
%
%
% MIT License
% 
% Copyright (c) 2021 Viktorio S. el Hakim
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

[n,N] = size(P);
lpopts = optimoptions('linprog','Algorithm','dual-simplex','Display','none','OptimalityTolerance',1.0000e-8);
pfac = pi/8; %Perturbation factor
perturb = true;

%Generate indices
Idx = cell(n+1,1);
for i = 1:n+1
    Idx{i} = setdiff(1:n+1,i);
end

%Initial simplex
H = gsimp(n)';
%Perturb the simplex
if perturb
    H = H*rrot(pfac,n)';
end
b = max(H*P,[],2);

%Enumerate its vertices
V = zeros(n,length(b));
% figure(1)
% plot(0,0)
% hold on
for i = 1:length(b)
    V(:,i) = H(Idx{i},:)\b(Idx{i});
%     plot_plane2d(H(i,:)',b(i),i,10);
end
% plot(V(1,:),V(2,:),'bo');
% hold off


%% Algorithm begin
N = size(P,2);

[fval,i] = min(b);
xopt = H(i,:)';
% fvals = zeros(1,Niter);
for k = 1:Niter
    %New simplex
    Hnew = H;
    bnew = b;

    %Perform cuts on each vertex (separate it from the points)
    for i = 1:length(b)
        % max f'x s.t. s_i^Tx >= 1
        f = V(:,i);
        x = linprog(f, [(P-V(:,i))';(V(:,Idx{i}) - V(:,i))';-V(:,i)'],[-ones(N+n,1);1],[],[],[],[],[],lpopts);
        if isempty(x)
            mindist = min(sum(abs(P-V(:,i)),1));
            error(['Infeasible solution found: most likely vertex too close to points, with min. dist ',num2str(mindist)]);
        end
        bnew(i) = (x'*V(:,i) - 1)/norm(x);
        Hnew(i,:) = x'/norm(x);
    end
    
    [fval2,i] = min(bnew);
    if(fval2 < fval)
        fval = fval2;
        xopt = Hnew(i,:);
    end
    
    %Check if simplex is well conditioned
    for i = 1:length(bnew)
        %Generate new random simplex if this one is ill-conditioned
        if cond(Hnew(Idx{i},:)) > 100
            Hnew = gsimp(n)';
            Hnew = Hnew*rrot(pi,n)';
            bnew = max(Hnew*P,[],2);
            [fval2,i] = min(bnew);
            if(fval2 < fval)
                fval = fval2;
                xopt = Hnew(i,:);
            end
            break
        end
    end
    
            
%     fvals(k) = fval;
%     figure(1)
%     plot(P(1,:),P(2,:),'ro');
%     hold on
    for i = 1:length(bnew)
        V(:,i) = Hnew(Idx{i},:)\bnew(Idx{i});
%         plot_plane2d(Hnew(i,:)',bnew(i),i,10);
    end
%     plot(V(1,:),V(2,:),'bo');
    H = Hnew;
    b = bnew;
%     hold off
end
% figure(2)
% plot(fvals);

end

function U = rrot( pfac, n )
    %Indexing pattern for off-diagonal entries of matrix
    idx = nchoosek(1:n,2);
    dU = zeros(n);
    dth = pfac*(2*rand(n*(n-1)/2,1) - 1); %Uniform
%     dth = pfac*randn(n*(n-1)/2,Npart);
    dU(sub2ind([n,n],idx(:,1),idx(:,2))) = dth;
    dU(sub2ind([n,n],idx(:,2),idx(:,1))) = -dth;
    U = expm(dU);
end

function [ simplex, a ] = gsimp( n )
dp = -1/n;

simplex = zeros(n,n+1);

simplex(1,1) = 1;
simplex(1,2:end) = dp;

sum_sq = dp*dp;
for i = 2:n
    v = sqrt(1 - sum_sq);
    simplex(i,i) = v;
    simplex(i,i+1:end) = (dp - sum_sq)/v;
    
    sum_sq = sum_sq + simplex(i,i+1)*simplex(i,i+1);
end

a = sqrt(2 + 2/n);

end





