function plot_plane2d( n,n0,k, range )
%plot_plane2d Draw a plot_plane2d in 2D
%   Plane equation is n'x == n0, where n is unit normal to the plane
%     k - plane number
%     range - length of plane line
%
% MIT License
% 
% Copyright (c) 2021 Viktorio S. el Hakim
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.

if nargin < 4
    range = 5;
end
x0 = n*n0/norm(n)^2;
t = linspace(-range,range,1000);
V = [n(2);-n(1)];
V = V/norm(V);
H_n = n/norm(n);
L = t.*V + x0;

plot(L(1,:),L(2,:),'-b');
text(x0(1),x0(2),['$H_{',num2str(k),'}$'],'Fontsize',14,'Interpreter','LaTeX');

hold_ch = ishold;
hold on
plot([x0(1),x0(1) + H_n(1)],[x0(2),x0(2) + H_n(2)],'-go');
if ~hold_ch
    hold off
end

end

