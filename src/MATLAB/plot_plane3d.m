function plot_plane3d( n,n0,k, R )
%plot_plane2d Draw a plot_plane2d in 3D
%   Plane equation is n'x == n0, where n is unit normal to the plane
%     k - plane number
%     R - area of plane
%
% MIT License
% 
% Copyright (c) 2021 Viktorio S. el Hakim
% 
% Permission is hereby granted, free of charge, to any person obtaining a copy
% of this software and associated documentation files (the "Software"), to deal
% in the Software without restriction, including without limitation the rights
% to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
% copies of the Software, and to permit persons to whom the Software is
% furnished to do so, subject to the following conditions:
% 
% The above copyright notice and this permission notice shall be included in all
% copies or substantial portions of the Software.
% 
% THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
% IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
% FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
% AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
% LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
% OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
% SOFTWARE.
if nargin < 4
    R = 5;
end

x0 = n*n0/norm(n)^2;
t = linspace(-R,R,50);
V = null(n');
V = normc(V);
H_n = n/norm(n);
[tx,ty] = meshgrid(t,t);
H = V*[tx(:),ty(:)]' + x0;

Hx = reshape(H(1,:),50,50);
Hy = reshape(H(2,:),50,50);
Hz = reshape(H(3,:),50,50);

surf(Hx,Hy,Hz);
text(H(1,end),H(2,end),H(3,end),['$H_{',num2str(k),'}$'],'Fontsize',18,'Interpreter','LaTeX');

hold_ch = ishold;
hold on
plot3([x0(1),x0(1) + H_n(1)],[x0(2),x0(2) + H_n(2)],[x0(3),x0(3) + H_n(3)],'-go');
if ~hold_ch
    hold off
end

end

