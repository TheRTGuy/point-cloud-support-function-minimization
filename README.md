# Point-cloud Support Function Minimization

MATLAB (and maybe others) code to minimize the support function of an n-dimensional point cloud, i.e.

```math
\min_x \quad f(x) = \max\{c_1^\top x,\ldots,c_k^\top x\} \quad \text{subject to}\quad \lVert x\rVert_2 = 1.
```

The approach uses an iterative successive simplex overapproximation technique, which relies on solving a set of LPs. The point cloud must satisfy
```math
0 \in \operatorname{Conv}(\{c_1,\ldots,c_k\}),
```
or in other words $`f(x) \geq 0`$ for all $`x`$. This can easily be tested by solving an LP.

The main solver function file is `minsf_pc.m`, and requires at least MATLAB 2016b. For now, the code uses the native `linprog` to solve the LPs. Later, I plan to switch to GLPK, or CLP. Even later on the road, it may even be possible to write an efficient LP solver for this specific problem.

I also plan to make Julia and Python extensions of the code.

The approach can be evaluated using the scripts `sf_min_pt_cloud_eval.m` and `sf_min_pt_cloud_eval2.m`. 

The first one generates a randomly rotated, uniformly distributed point cloud with $`k=1000`$, and evaluates the algorithm for each $`n=2,\ldots,20`$. You can play around with it as you please to evaluate it with other configurations: for example, I also have commented out a configuration where I mix a uniformly and a normally distributed cloud. Also, I generate $`1000000`$ normally distributed points on the unit hypersphere to measure the 'optimality' of the solutions. The number of iterations is fixed for all runs.

The second script is essentially the same as the first one, with the difference that instead of generating random points on the unit sphere to measure optimality, I instead compute the difference of the objective value with $`L=5`$, which is the side length of a randomly rotate hypercube, wherein the uniformly distributed point cloud is rotated. A good objective value must always satisfy $`f^*\leq L/2`$. Also, I set the number of iterations for each run to $`N = 4 + \lfloor pn\rfloor`$, where $`p=1.2`$. 

An improvement to the speed of the approach is allowing to solve the LPs in parallel. Actually, this can be done easily by changing line 
```MATLAB
for i = 1:length(b)
```
to
```MATLAB
parfor i = 1:length(b)
```
Of course, you need to setup the parallel computing toolbox for that.
